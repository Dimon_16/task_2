﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;


namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();
        public ActionResult Index()
        {
            var postman = new Post()
            {
                Name = "Dimon",
                Description = "seter"
            };

            db.Posts.Add(postman);
            db.SaveChanges();

            var pos = db.Posts.ToList();
            
            return View(pos);
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return Redirect("/Home/Index");
        }
    }
}