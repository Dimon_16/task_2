﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Post
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
    }
}